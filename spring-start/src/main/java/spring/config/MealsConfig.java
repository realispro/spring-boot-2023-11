package spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;

@Configuration
@PropertySource("classpath:/meals.properties")
public class MealsConfig {

    @Bean("meals")
    List<String> meals(@Value("#{'${meal.menu}'.split(',')}") List<String> menu){
        return new ArrayList<>(menu);
    }
}
