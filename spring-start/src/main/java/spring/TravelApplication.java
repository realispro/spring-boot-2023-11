package spring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;

@SpringBootApplication
public class TravelApplication {

    public static void main(String[] args) {
        SpringApplication.run(TravelApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(Travel travel){
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                Person kowalski = new Person("Jan", "Kowalski", new Ticket(LocalDate.now().minusDays(1)));
                travel.travel(kowalski);
            }
        };
    }

}
