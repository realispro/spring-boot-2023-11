package vod.web;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/webapi")
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas") // /cinemas?foo=fooValue&bar=barValue
    List<CinemaDTO> getCinemas(
            @RequestParam(value = "foo", required = false) String foo,
            @RequestHeader(value = "barHeader", required = false) String bar,
            @RequestHeader Map<String, String> headers,
            @CookieValue(value = "xyz", required = false) String cookie
            ){
        log.info("about to retrieve cinemas");
        log.info("request param foo: {}", foo);
        log.info("request header bar: {}", bar);
        log.info("request headers: {}", headers);
        log.info("cookie value: {}", cookie);

        return cinemaService.getAllCinemas()
                .stream()
                .map(cinema->CinemaDTO.fromData(cinema))
                .toList();
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDTO> getCinema(@PathVariable("cinemaId") int cinemaId){
        log.info("about to retrieve cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if(cinema!=null) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("foo", "bar")
                    .body(CinemaDTO.fromData(cinema));
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }


    @GetMapping("/movies/{movieId}/cinemas")
    List<CinemaDTO> getCinemasShowingMovie(@PathVariable("movieId") int movieId){
        log.info("about to retrieve cinemas showing movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        if(movie!=null) {
            List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
            return cinemas.stream().map(CinemaDTO::fromData).toList();
        } else {
            return List.of();
        }
    }

    @Data
    static class CinemaDTO {
        private int id;
        private String name;
        private String logo;

        static CinemaDTO fromData(Cinema cinema){
            CinemaDTO dto = new CinemaDTO();
            dto.setId(cinema.getId());
            dto.setName(cinema.getName());
            dto.setLogo(cinema.getLogo());
            return dto;
        }
    }
}
