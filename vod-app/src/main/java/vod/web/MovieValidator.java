package vod.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.model.Director;
import vod.service.MovieService;

@Component
@RequiredArgsConstructor
public class MovieValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(MovieController.MovieDTO.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieController.MovieDTO dto = (MovieController.MovieDTO) target;
        Director director = movieService.getDirectorById(dto.getDirectorId());
        if(director==null){
            errors.rejectValue("directorId", "errors.director.missing");
        }
    }
}
