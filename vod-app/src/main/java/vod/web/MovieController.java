package vod.web;


import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Director;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.net.URI;
import java.util.*;


@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/webapi")
public class MovieController {

    private final MovieService movieService;
    private final CinemaService cinemaService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;
    private final MovieValidator movieValidator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.addValidators(movieValidator);
    }

    @GetMapping("/movies")
    List<MovieDTO> getMovies() {
        log.info("About to retrieve movies");
        return movieService.getAllMovies()
                .stream()
                .map(movie -> MovieDTO.fromData(movie))
                .toList();
    }

    @GetMapping("/movies/{movieId}")
    ResponseEntity<MovieDTO> getMovie(@PathVariable("movieId") int movieId) {
        log.info("About to retrieve movie {}",movieId);

        Movie movie  = movieService.getMovieById(movieId);
        if (movie != null) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(MovieDTO.fromData(movie));
        } else {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }

    @GetMapping("/cinemas/{cinemaId}/movies")
    List<MovieDTO> getMoviesShowingByCinema(@PathVariable int cinemaId) {
        log.info("about to retrieve movies showing by cinema.id={}", cinemaId);
        return Optional.ofNullable(cinemaService.getCinemaById(cinemaId))
                .map(c -> cinemaService.getMoviesInCinema(c))
                .stream()
                .flatMap(Collection::stream)
                .map(MovieDTO::fromData)
                .toList();
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(
            @Validated @RequestBody MovieDTO movieDTO,
            Errors errors,
            HttpServletRequest request){
        log.info("about to add movie: {}", movieDTO);

        if(errors.hasErrors()){
            Locale locale = localeResolver.resolveLocale(request);
            List<String> labels = errors.getAllErrors().stream()
                    .map(objectError -> messageSource.getMessage(
                            objectError.getCode(),
                            objectError.getArguments(),
                            locale
                            ))
                    .toList();

            return ResponseEntity.badRequest().body(labels);
        }

        Movie movie = movieService.addMovie(movieDTO.toData());
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{movieId}")
                .build(Map.of(
                        "movieId", movie.getId()
                ));

        return ResponseEntity
                //.status(HttpStatus.CREATED)
                .created(uri)
                .build();
    }


    @Data
    static class MovieDTO {
        private int id;
        @NotNull
        @Size(min = 2, max=100)
        private String title;
        @NotNull
        //@Pattern()
        private String poster;
        private int directorId;


        static MovieDTO fromData(Movie movie) {
            MovieDTO dto = new MovieDTO();
            dto.setId(movie.getId());
            dto.setTitle(movie.getTitle());
            dto.setPoster(movie.getPoster());
            dto.setDirectorId(movie.getDirector().getId());

            return dto;
        }

        Movie toData(){
            Movie movie = new Movie();
            movie.setId(this.id);
            movie.setTitle(this.title);
            movie.setPoster(this.poster);
            Director director = new Director();
            director.setId(this.directorId);
            movie.setDirector(director);
            return movie;
        }

    }
}
