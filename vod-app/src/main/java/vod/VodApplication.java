package vod;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import vod.model.Cinema;
import vod.model.Movie;
import vod.model.Rating;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;

@SpringBootApplication
@Slf4j
public class VodApplication {

    public static void main(String[] args) {
        SpringApplication.run(VodApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(CinemaService cinemaService, MovieService movieService){
        return args -> {
            List<Cinema> cinemas = cinemaService.getAllCinemas();
            log.info(cinemas.size() + " cinemas found:");
            cinemas.forEach(System.out::println);

            Movie movie = movieService.getMovieById(2);
            List<Rating> ratings = movieService.getRatingsByMovie(movie);
            log.info("Rating of movie {}", movie.getTitle());
            ratings.forEach(rating -> log.info("rate: {}", rating.getRate()));
        };
    }
}
