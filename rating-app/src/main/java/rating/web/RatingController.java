package rating.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rating.model.Rating;
import rating.repository.RatingRepository;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RatingController {

    private final RatingRepository ratingRepository;
    private final RatingValidator ratingValidator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.addValidators(ratingValidator);
    }

    @GetMapping("/ratings")
    List<Rating> getRatings(@RequestParam(value = "movieId", required = false) Integer movieId){
        log.info("about to retrieve ratings of movie {}", movieId);
        return movieId!=null
                ? ratingRepository.findAllByMovieId(movieId)
                : ratingRepository.findAll();
    }

    @PostMapping("/ratings")
    ResponseEntity<?> addRating(@Validated @RequestBody Rating rating, Errors errors){
        log.info("about to add rating {}", rating);

        if(errors.hasErrors()){
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }

        rating = ratingRepository.save(rating);

        return ResponseEntity.status(201).build();

    }

}
