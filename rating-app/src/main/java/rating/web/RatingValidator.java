package rating.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import rating.model.Rating;

import java.util.Map;

@Component
public class RatingValidator implements Validator {

    @Value("${vod.service.url}")
    private String vodServiceUrl;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(Rating.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Rating rating = (Rating) target;
        //rating.getMovieId()

        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<Map> responseEntity = restTemplate.exchange(
                    vodServiceUrl + "/movies/" + rating.getMovieId(),
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    Map.class
            );
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                errors.rejectValue("movieId", "errors.movie.missing");
            }
        } catch (HttpClientErrorException e){
            errors.rejectValue("movieId", "errors.movie.service.exception");
        }
    }
}
