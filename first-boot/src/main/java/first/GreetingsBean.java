package first;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GreetingsBean {

    @Value("${greetings.text:Hey Joe!}")
    private String greetingText;

    @PostConstruct
    private void postConstruct(){
        log.info("creating greetings bean...");
    }

    public String sayHello(){
        return greetingText;
    }
}
